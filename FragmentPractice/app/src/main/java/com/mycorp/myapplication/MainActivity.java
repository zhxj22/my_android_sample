package com.mycorp.myapplication;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.mycorp.myapplication.Fragment1;
import com.mycorp.myapplication.Fragment2;


public class MainActivity extends ActionBarActivity implements
        Fragment1.OnFragmentInteractionListener,
        View.OnClickListener,
        Fragment2.OnFragmentInteractionListener{

    private static String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.remove1).setOnClickListener(this);
        findViewById(R.id.remove2).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(this, "interface not implemented", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button1:
                if (getSupportFragmentManager().getFragments()!=null)
                {
                    int n = getSupportFragmentManager().getFragments().size();
                    Log.d(TAG, String.valueOf(n));
                }
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_container, new Fragment1(), "")
                        .addToBackStack(null)
                        .commit();
                break;

            case R.id.button2:
                getSupportFragmentManager().beginTransaction().add(R.id.frame_container, new Fragment2(), "").commit();
                break;

            case R.id.remove1:
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                break;

            case R.id.remove2:
                //getSupportFragmentManager().popBackStack();
                int n = getSupportFragmentManager().getBackStackEntryCount();
                for (int i=0; i<n; i++)
                {
                    getSupportFragmentManager().popBackStack();
                }
                break;

            default:
                break;
        }
    }
}
