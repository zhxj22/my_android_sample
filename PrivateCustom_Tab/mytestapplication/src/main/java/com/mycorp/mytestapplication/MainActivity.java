package com.mycorp.mytestapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mycorp.mytestapplication.app.AppController;

import org.json.JSONObject;

import javax.xml.transform.ErrorListener;


public class MainActivity extends Activity implements View.OnClickListener {

    private static String TAG = "MainActivity";
    Spinner mSpinner;
    PopupWindow mPopupWindow;
    View mView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSpinner = (Spinner) findViewById(R.id.myspinner);
        String[] names = getResources().getStringArray(R.array.names);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, names);
        mSpinner.setAdapter(arrayAdapter);
        mView = getLayoutInflater().inflate(R.layout.my_pop_up_layout, null);
        mPopupWindow = new PopupWindow(mView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.setWidth(button.getWidth());
                mPopupWindow.showAsDropDown(button);
            }
        });
        mPopupWindow.getContentView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.dismiss();
            }
        });

        myVolleyTest();

        findViewById(R.id.button_json).setOnClickListener(this);
    }

    public void myVolleyTest()
    {
        String url ="http://www.baidu.com";
        //获取一个请求队列
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        //定义一个字符串型请求队列，需要传递4个参数分别是：url, 请求的方式(get, post..)，响应成功的处理函数， 相应出错时候的处理函数
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //这里可以直接更新UI
                        System.out.println(s);
                        VolleyLog.d("Volley", s);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //这里可以直接更新UI
                        System.out.println(volleyError);
                    }
        });

        queue.add(stringRequest);//add之后就开始执行
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        String url = "http://api.androidhive.info/volley/person_object.json";

        switch (v.getId())
        {
            case R.id.button_json:

                final ProgressDialog pDialog = new ProgressDialog(this);
                pDialog.setMessage("Loading...");
                pDialog.show();

                JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                        url, null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d(TAG, response.toString());
                                pDialog.hide();
                            }
                        },
                        new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d(TAG, "Error: " + error.getMessage());
                                // hide the progress dialog
                                pDialog.hide();
                            }
                });

                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
                break;
            default:
                ImageRequest imageRequest = new ImageRequest("",
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {
                                System.out.println(response);
                                ((ImageView)findViewById(R.id.image)).setImageBitmap(response);
                            }
                        }, 100, 100, Bitmap.Config.ALPHA_8,
                        new Response.ErrorListener(){
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                System.out.println(error);
                            }
                        });
                AppController.getInstance().addToRequestQueue(imageRequest);
                break;
        }

    }
}
