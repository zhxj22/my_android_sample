package com.mycorp.intentservicepractice;

/**
 * Created by xzhao23 on 5/20/2015.
 */
public class Constants {
    // Defines a custom Intent action
    public static final String BROADCAST_ACTION =
            "com.example.android.threadsample.BROADCAST";
    // Defines the key for the status "extra" in an Intent
    public static final String EXTENDED_DATA_STATUS =
            "com.example.android.threadsample.STATUS";
}
