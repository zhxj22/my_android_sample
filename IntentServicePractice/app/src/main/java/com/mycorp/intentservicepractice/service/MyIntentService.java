package com.mycorp.intentservicepractice.service;

import android.app.IntentService;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.util.Log;

import com.mycorp.intentservicepractice.Constants;

/**
 * Created by xzhao23 on 5/20/2015.
 */
public class MyIntentService extends IntentService {

    private static String TAG = "MyIntentService";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "FINISH.......");
    }
}
